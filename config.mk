#
# Copyright (c) 2011 Atmel Corporation. All rights reserved.
#
# \asf_license_start
#
# \page License
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. The name of Atmel may not be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# 4. This software may only be redistributed and used in connection with an
#    Atmel microcontroller product.
#
# THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
# EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# \asf_license_stop
#

# Path to top level ASF directory relative to this project directory.
PRJ_PATH = ./

# Target CPU architecture: cortex-m3, cortex-m4
ARCH = cortex-m4

# Target part: none, sam3n4 or sam4l4aa
PART = SAM4SD32C
#PART = SAM4S4A
# Application target name. Given with suffix .a for library and .elf for a
# standalone application.
TARGET_FLASH = SAM4S-ECM-CDC-master.elf
TARGET_SRAM = SAM4S-ECM-CDC-master.elf

# List of C source files.
CSRCS = \
       lwip-2.1.2/src/core/def.c        \
       lwip-2.1.2/src/core/ipv4/dhcp.c          \
       lwip-2.1.2/src/core/dns.c       \
       lwip-2.1.2/src/core/init.c                           \
       lwip-2.1.2/src/core/mem.c \
       lwip-2.1.2/src/core/memp.c \
       lwip-2.1.2/src/core/netif.c   \
       lwip-2.1.2/src/core/pbuf.c                \
       lwip-2.1.2/src/core/raw.c                      \
	   lwip-2.1.2/src/core/stats.c \
	   lwip-2.1.2/src/core/sys.c \
	   lwip-2.1.2/src/core/tcp.c \
	   lwip-2.1.2/src/core/tcp_in.c \
	   lwip-2.1.2/src/core/tcp_out.c \
	   lwip-2.1.2/src/core/timers.c \
	   lwip-2.1.2/src/core/udp.c \
	   lwip-2.1.2/src/core/ip.c \
	   lwip-2.1.2/src/core/ipv4/autoip.c \
	   lwip-2.1.2/src/core/ipv4/icmp.c \
	   lwip-2.1.2/src/core/ipv4/igmp.c \
	   lwip-2.1.2/src/core/inet_chksum.c \
	   lwip-2.1.2/src/core/ipv4/ip4.c \
	   lwip-2.1.2/src/core/ipv4/ip4_addr.c \
	   lwip-2.1.2/src/core/ipv4/ip4_frag.c \
	   lwip-2.1.2/src/core/ipv4/etharp.c \
	   lwip-2.1.2/src/netif/ethernet.c \
	   lwip-2.1.2/src/apps/http/fs.c \
	   lwip-2.1.2/src/apps/http/httpd.c\
	   drivers/sysclk.c\
	   drivers/pmc.c\
	   drivers/pio.c\
	   drivers/efc.c\
	   drivers/flash_efc.c\
	   drivers/interrupt_sam_nvic.c \
	   drivers/sam_sleep.c \
	   project/app.c \
	   project/time.c \
	   USB_udp/udc.c \
	   USB_udp/udi_cdc_desc.c \
	   USB_udp/udi_cdc.c \
	   USB_udp/udp_device.c \
	   USB_udp/usb_ecm.c \
	   dhcp-server/dhserver.c \
	   dns-server/dnserver.c \
       utils/cmsis/sam4s/source/templates/gcc/startup_sam4s.c \
       utils/cmsis/sam4s/source/templates/system_sam4s.c     \
       utils/syscalls/gcc/syscalls.c 

# List of assembler source files.
ASSRCS = 

# List of include paths.
INC_PATH = \
       dhcp-server                                      \
       dns-server                                      \
       USB_udp                                      \
       lwip-2.1.2/src/include                      \
       lwip-2.1.2/src/include/arch  \
       lwip-2.1.2/src/include/ipv4                                       \
       lwip-2.1.2/src/include/ipv6                    \
       lwip-2.1.2/src/include/lwip                                  \
       lwip-2.1.2/src/include/netif                                \
       lwip-2.1.2/src/include/posix                          \
       lwip-2.1.2/src/include/lwip/apps  \
       lwip-2.1.2/src/apps/http  \
       project                      \
       drivers \
       utils                                         \
       utils/cmsis/sam4s/include                    \
       utils/cmsis/sam4s/include/component                    \
       utils/cmsis/sam4s/source                     \
       utils/header_files                            \
       utils/preprocessor                            \
       thirdparty/CMSIS/Include                           \
       thirdparty/CMSIS/Lib/GCC 

# Additional search paths for libraries.
LIB_PATH =  \
       thirdparty/CMSIS/Lib/GCC                          

# List of libraries to use during linking.
LIBS =  \
       arm_cortexM4l_math                                 \
       m                                

# Path relative to top level directory pointing to a linker script.
LINKER_SCRIPT_FLASH = utils/linker_scripts/sam4s/sam4sd32/gcc/flash.ld
#LINKER_SCRIPT_SRAM  = utils/linker_scripts/sam4s/sam4sd32/gcc/sram.ld


# Project type parameter: all, sram or flash
PROJECT_TYPE        = flash

# Additional options for debugging. By default the common Makefile.in will
# add -g3.
DBGFLAGS = 

# Application optimization used during compilation and linking:
# -O0, -O1, -O2, -O3 or -Os
OPTIMIZATION = -O2

# Extra flags to use when archiving.
ARFLAGS = 
 
# Extra flags to use when assembling.
ASFLAGS = 

# Extra flags to use when compiling.
CFLAGS = 

# Extra flags to use when preprocessing.
#
# Preprocessor symbol definitions
#   To add a definition use the format "-D name[=definition]".
#   To cancel a definition use the format "-U name".
#
# The most relevant symbols to define for the preprocessor are:
#   BOARD      Target board in use, see boards/board.h for a list.
#   EXT_BOARD  Optional extension board in use, see boards/board.h for a list.
CPPFLAGS = \
       -D ARM_MATH_CM4=true                               \
       -D LWIP_HTTPD_STRNSTR_PRIVATE=0					\
       -D CHIP_FREQ_XTAL=16000000UL						\
       -D CONF_BOARD_SAM4S_EK2

#	   -D CONF_BOARD_PD956
# Extra flags to use when linking
LDFLAGS = \

# Pre- and post-build commands
PREBUILD_CMD = 
POSTBUILD_CMD = 