/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Peter Lawrence
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <string.h>

#include "udi_cdc.h"
#include "usb_ecm.h"

#define ECM_MAX_SEGMENT_SIZE           1514

static COMPILER_WORD_ALIGNED uint8_t received[ECM_MAX_SEGMENT_SIZE];
static COMPILER_WORD_ALIGNED uint8_t transmitted[ECM_MAX_SEGMENT_SIZE];

static bool can_xmit;

static void usb_ecm_ep_send_callback(int size);
static void usb_ecm_ep_recv_callback(int size);

void usb_ecm_init(void)
{

}

static void usb_ecm_send(uint8_t *data, int size)
{
  //usb_send(USB_ECM_EP_SEND, data, size);
	udi_cdc_write_buf(data, size);
}
static int collected;
void rx_poll(void)
{
	int size;


	size = udi_cdc_get_nb_received_data();

	collected += udi_cdc_read_no_polling(&received[collected], size);

	if((size != 64)){
		usb_ecm_ep_recv_callback(collected);
		collected = 0;
	}
}
void usb_ecm_recv_renew(void)
{
  //usb_recv(USB_ECM_EP_RECV, received, sizeof(received));
  //udi_cdc_read_buf(received, sizeof(received));

  udi_cdc_read_no_polling(received, sizeof(received));
}

void usb_configuration_callback(int config)
{
  (void)config;

  usb_ecm_recv_renew();
  can_xmit = true;
}
void tx_empty_poll(void)
{
	usb_ecm_ep_send_callback(0);
}

static void usb_ecm_ep_send_callback(int size)
{
  (void)size;

  can_xmit = true;
}

static void usb_ecm_ep_recv_callback(int size)
{
  usb_ecm_recv_callback(received, size);
}

bool usb_ecm_can_xmit(void)
{
  return can_xmit;
}

void usb_ecm_xmit_packet(struct pbuf *p)
{
  struct pbuf *q;
  uint8_t *data;
  int packet_size;

  if (!can_xmit)
    return;

  data = transmitted;
  packet_size = 0;
  for(q = p; q != NULL; q = q->next)
  {
    memcpy(data, (char *)q->payload, q->len);
    data += q->len;
    packet_size += q->len;
  }

  can_xmit = false;
  usb_ecm_send(transmitted, packet_size);
}

#define PACKET_TYPE_PROMISCUOUS		(1<<0)
#define PACKET_TYPE_ALL_MULTICAST	(1<<1)
#define PACKET_TYPE_DIRECTED		(1<<2)
#define PACKET_TYPE_BROADCAST		(1<<3)
#define PACKET_TYPE_MULTICAST		(1<<4)

/*//// Device specific request types
 * #define  USB_REQ_CDC_SET_ETHERNET_MULTICAST_FILTERS              0x40
 * #define  USB_REQ_CDC_SET_ETHERNET_POWER_MANAGEMENT_PATTERNFILTER 0x41
 * #define  USB_REQ_CDC_GET_ETHERNET_POWER_MANAGEMENT_PATTERNFILTER 0x42
 * #define  USB_REQ_CDC_SET_ETHERNET_PACKET_FILTER                  0x43
 * #define  USB_REQ_CDC_GET_ETHERNET_STATISTIC                      0x44
 */
static
void cdc_ecm_set_ethernet_packet_filter(uint16_t usb_ecm_packet_filter)
{

	if(usb_ecm_packet_filter & PACKET_TYPE_PROMISCUOUS) {
		//PRINTF_P(PSTR("PROMISCUOUS "));
		//USB_ETH_HOOK_SET_PROMISCIOUS_MODE(true);
	} else {
		//USB_ETH_HOOK_SET_PROMISCIOUS_MODE(false);
	}

	/*if(usb_ecm_packet_filter & PACKET_TYPE_ALL_MULTICAST)
		PRINTF_P(PSTR("ALL_MULTICAST "));
	if(usb_ecm_packet_filter & PACKET_TYPE_DIRECTED)
		PRINTF_P(PSTR("DIRECTED "));
	if(usb_ecm_packet_filter & PACKET_TYPE_BROADCAST)
		PRINTF_P(PSTR("BROADCAST "));
	if(usb_ecm_packet_filter & PACKET_TYPE_MULTICAST)
		PRINTF_P(PSTR("MULTICAST "));

	PRINTF_P(PSTR("\n"));*/
}


char usb_device_specific_request(void)
{
	//uint16_t wLength = udd_g_ctrlreq.req.wLength;
	uint16_t request = udd_g_ctrlreq.req.bRequest;
	uint16_t value = udd_g_ctrlreq.req.wValue;

	switch(request)
	{
		case USB_REQ_CDC_SET_ETHERNET_PACKET_FILTER:

			cdc_ecm_set_ethernet_packet_filter(value);
			return 1;
			break;

     	default:
			break;

	}

  	return 0;
}
